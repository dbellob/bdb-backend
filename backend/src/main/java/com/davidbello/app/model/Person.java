package com.davidbello.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author David Bello
 *
 */

@Entity
@Table(name = "person")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String fullname;
	private Date birth;
	private int idfather;
	private int idmather;
	private int idchild;

	public Person() {
		super();
	}

	public Person(int id, String fullname, Date birth, int idfather, int idmather, int idchild) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.birth = birth;
		this.idfather = idfather;
		this.idmather = idmather;
		this.idchild = idchild;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public int getIdfather() {
		return idfather;
	}

	public void setIdfather(int idfather) {
		this.idfather = idfather;
	}

	public int getIdmather() {
		return idmather;
	}

	public void setIdmather(int idmather) {
		this.idmather = idmather;
	}

	public int getIdchild() {
		return idchild;
	}

	public void setIdchild(int idchild) {
		this.idchild = idchild;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", fullname=" + fullname + ", birth=" + birth + ", idfather=" + idfather
				+ ", idmather=" + idmather + ", idchild=" + idchild + "]";
	}
}
