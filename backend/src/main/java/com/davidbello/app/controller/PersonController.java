package com.davidbello.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.davidbello.app.model.Person;
import com.davidbello.app.repository.PersonRepository;;

/**
 * 
 * @author David Bello
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class PersonController {

	@Autowired
	private PersonRepository repository;
	
	
	@CrossOrigin(origins = "*")
	@GetMapping("/person")
	public List<Person> getAllLetters() {
		return repository.findAll();
	}
	
	
	
	@PostMapping("/person/save")
	public ResponseEntity<Boolean> obtenerCriteriosBusquedaDocumentosUsuario(
			
			@Valid @RequestBody Person person) {
	
		try {
			repository.save(person);
			return new ResponseEntity<>(true, HttpStatus.OK);

		} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
