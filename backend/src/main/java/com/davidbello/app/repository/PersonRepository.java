package com.davidbello.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.davidbello.app.model.Person;

/**
 * 
 * @author David Bello
 *
 */

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {
}