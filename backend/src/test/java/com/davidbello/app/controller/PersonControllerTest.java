package com.davidbello.app.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.davidbello.app.model.Person;
import com.davidbello.app.repository.PersonRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonControllerTest {

    @Autowired
    private PersonRepository repository;

    @Test
    public void testPersonRepository() {

        Person p1 = new Person(1, "David Bello", new Date(), 0, 0, 0); 
        	repository.save(p1);
        assertNotNull(p1);
        Optional<Person> p2 = repository.findById(1);
        assertNotNull(p2);
        assertEquals(p1.getFullname(), p2.get().getFullname());
        
        
    }

  
}