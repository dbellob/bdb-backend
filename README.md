# bdb backend

Ejecutar los siguientes pasos en el orden en que se describen a continuación:

## Pasos para configurar BD con Docker
En este caso la base de datos a utilizar es postgres.
1. Contar con docker desktop instalado
2. En una terminal ejecutar lo siguiente: 

> docker run --name bdb -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres
3. Realice la conexión a la base de datos con los siguientes parámetros por defecto: 
>   Host: localhost

>  Database: postgres
   
> User: postgres

>Password: postgres
   
>Port: 5432

Para la conexión se sugiere utilizar la siguiente herramienta: https://dbeaver.io/download/ 

4. Ejecute el siguiente script SQL:
> CREATE TABLE public.person (
  id              SERIAL PRIMARY KEY,
  fullname         text NOT NULL,
  birth           date not null,
  idfather          int default 0,
  idmather          int default 0,
  idchild			int default 0
);

## Pasos para ejecutar API REST
1. Contar con maven instalado y configurado: https://maven.apache.org/install.html
2. Contar con Java cofigurado con la siguiente versión:
  java version "1.8.0_221"
  Java(TM) SE Runtime Environment (build 1.8.0_221-b11)
  Java HotSpot(TM) 64-Bit Server VM (build 25.221-b11, mixed mode)
3. Sobre la carpeta backend ejecutar el comando: 
> mvn clean install
4. Sobre la carpeta backend/target ejecutar el API REST a través del comando:  
> java -jar backend-0.0.1-SNAPSHOT.jar
5. Testar los dos enpoints con el proyecto POSTMAN person.postman_collection.json